﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace GeoWebService.Model
{
    public class GeoIpContext : DbContext
    {
        public DbSet<Country> Countries { get; set; }
        public DbSet<Ip> Ips { get; set; }

        public GeoIpContext()
        {

        }

        public GeoIpContext(DbContextOptions<GeoIpContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=geoip.db");
        }
        public static void Init()
        {
            using (var db = new GeoIpContext())
            {
                db.Database.Migrate();

                if (db.Ips.Count() < 1)
                {

                    Country[] countries = new Country[3]
                    {
                   new Country() {Name = "Russia" },
                   new Country() {Name = "USA" },
                   new Country() {Name = "China" }
                    };

                    List<Ip> ipies = new List<Ip>();
                    ipies.Add(new Ip() { Value = "000.000.000.000", Country = countries[0] });
                    ipies.Add(new Ip() { Value = "000.000.000.001", Country = countries[1] });
                    ipies.Add(new Ip() { Value = "000.000.000.002", Country = countries[2] });
                    ipies.Add(new Ip() { Value = "000.000.000.003", Country = countries[0] });
                    ipies.Add(new Ip() { Value = "000.000.000.004", Country = countries[1] });
                    ipies.Add(new Ip() { Value = "000.000.000.005", Country = countries[2] });

                    foreach (Ip ip in ipies)
                    {
                        db.Add(ip);
                    }
                    db.SaveChanges();
                }
            }
        }
    }
}
