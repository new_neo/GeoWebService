﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GeoWebService.Model;
using Microsoft.EntityFrameworkCore;

namespace GeoWebService.Controllers
{
    public class HomeController : Controller
    {
        class CountryResult
        {
            public string country = "";
            public string error = "";
            public object ToJson()
            {
                return new {
                    country = country,
                    error = error
                }; 
            }
        }
        public IActionResult Error()
        {
            return View();
        }
        public IActionResult FormCountryByIp()
        {
            return View();
        }

        public IActionResult GetCountry(string ipCheck)
        {
            return Json(CountryByIp(ipCheck).ToJson());
        }

       
        [HttpPost]
        public IActionResult GetCountryByIp([FromForm] string ipCheck)
        {
            var countryResult = CountryByIp(ipCheck);
            if (string.IsNullOrEmpty(countryResult.country))
            {
                if(string.IsNullOrEmpty(countryResult.error))
                {
                    ViewData["Country"] = "Информация об этом ip отсутствует";
                }
                else
                {
                    ViewData["Country"] = countryResult.error;
                }
            }
            else
            {
                ViewData["Country"] = CountryByIp(ipCheck).country;
            }
            return View();
        }

        private CountryResult CountryByIp(string ipCheck)
        {
            CountryResult countryResult = new CountryResult();
            using (var db = new GeoIpContext())
            {
                try
                {
                    countryResult.country = db.Ips.Include((ip) => ip.Country).FirstOrDefault(
                        (ip) =>
                            ip.Value == ipCheck
                        )?.Country
                        ?.Name;
                }
                catch (Exception ex)
                {
                    countryResult.error = ex.Message;
                }

                return countryResult;
            }
        }
    }
}
